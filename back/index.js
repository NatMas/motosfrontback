
const express = require("express");

const cors = require('cors');
const bodyParser = require('body-parser');
const { request, response } = require("express");
const app = express();
const baseUrl = '/miapi';
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
ddbbConfig = {
    user: 'natalia-mas-7e3',
    host: 'postgresql-natalia-mas-7e3.alwaysdata.net',
    database: 'natalia-mas-7e3_motos',
    password: 'Contraseña1',
    port: 5432 };
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

const getMotos = (request, response) => {
         var consulta = "SELECT * FROM  motos"
         pool.query(consulta, (error, results) => {
              if (error) {
            throw error
        }
        response.status(200).json(results.rows)
        console.log(results.rows);    
    }); }
app.get(baseUrl + '/motos', getMotos);

const addMoto = (request, response) => {    
    console.log(request.body);    
    const { marca, modelo, year, foto, precio } = request.body
    console.log(marca, modelo, year, foto, precio);
    var consulta = `INSERT INTO motos (marca,modelo,year,foto,precio) VALUES ('${marca}', '${modelo}','${year}','${foto}','${precio}')`
    pool.query(consulta, (error) => {
        if (error) {
    throw error
  }
  }); 
}

 app.post(baseUrl + '/moto', addMoto);

const filtreMoto = (request,response) =>{
    const marca = request.query.marca
    var consulta = `SELECT * FROM  motos where marca = '${marca}' `
    pool.query(consulta, (error, results) => {
        if (error) {
      throw error
  }
  response.status(200).json(results.rows)
  console.log(results.rows);    
}); }
app.get(baseUrl + '/motosFilt', filtreMoto);


const deleteMoto = (request,response) =>{
    const id = request.query.id
    var consulta = `DELETE FROM MOTOS WHERE id ='${id}'`
    pool.query(consulta, (error, results) => {
        if (error) {
      throw error
  }
  response.status(200).json(results.rows)
  console.log(results.rows);    
});
}
app.delete(baseUrl +'/delete',deleteMoto)
//Inicialitzem el servei
const PORT = process.env.PORT || 3000;
const IP = process.env.IP || null;
app.listen(PORT, IP, () => {    console.log("El servidor está inicialitzat en el puerto " + PORT); });
